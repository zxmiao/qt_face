#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <myface.h>
#include "database.h"

using namespace cv;
using namespace std;
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
    int show_result(Employee &e, void *data);

private slots:
    void update();

private:
    Ui::Widget *ui;
    cv::VideoCapture cam; //摄像头拍摄到的一帧图像
    QTimer* timer;        //用于定时，时间到了会触发update函数
    MyFace *face;         //人脸检测，识别类
    Rect range;           //人脸区域
    int frame;            //用于统计相机的帧数
    int hit;              // 用于统计识别到人脸的数量
    Employee user;        //用户的结构体
    Databass *db;         //数据库类

};

#endif // WIDGET_H
