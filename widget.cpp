#include "widget.h"
#include "ui_widget.h"
#include <QDebug>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    cam(0),
    frame(0),
    hit(0)
{
    ui->setupUi(this);
    //初始化计时器
    timer = new QTimer(this);
    //绑定计时器的时间到了的事件，连接到刷新函数
    connect(timer, &QTimer::timeout, this, &Widget::update);
    //开启计时器
    timer->start(40);
    //设置使用的人脸检测模型
    std::string model = "/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml";
    face = new MyFace(model);
    db = new Databass();
}

int Widget::show_result(Employee &e, void *data)
{
    return 0;
}

void Widget::update()
{
    cv::Mat image;
    cam >> image;
    frame++;
    //每5帧检测一次人脸
    if((frame%5) == 0)
    {
        frame = 0;
        //获取人脸信息到range
        if(face->detect(image,range))
        {
            hit++;
        }else {
            hit = 0;
        }
    }
    //5次连续检测到人人脸才会调用百度api进行人脸识别
    if(5 == hit)
    {
        hit = 0;
        //根据人脸块的信息从照片中获取到较小的人脸图
        cv::Mat faceimg(image,range);
        //使用人脸识别接口，成功返回1
        if(face->recognize(faceimg, user))
        {
            //在ui中显示打卡成功的信息
            ui->label_result->setText("打卡成功");
            ui->label_showname->setText(QString::fromStdString(user.id));
            //时间暂时没用到，在数据库接口里有其他调用
            time_t t;
            db->punch(user.id, t);
        }
        //std::string name = db->getname(user.id);
    }
    //在图像上显示人脸的框
    cv::rectangle(image,range, CV_RGB(255,0,0));
    //格式的转换 让图像可以在qt中显示
    cv::cvtColor(image, image, CV_BGR2RGB);
    QImage qimage(image.data, image.cols, image.rows,
                  image.step, QImage::Format_RGB888);
    ui->label->setPixmap(QPixmap::fromImage(qimage));
}

Widget::~Widget()
{
    delete ui;
    //delete face;
}


