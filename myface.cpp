#include "myface.h"
#include <aip-cpp-sdk/face.h>
#include <QDebug>
#include <iostream>

using namespace std;


MyFace::MyFace(string model)
{
    //加载人脸检测模型
    classifier.load(model);

}

int MyFace::detect(cv::Mat image, cv::Rect &face)
{
    //处理图像
    cv::cvtColor(image,image, CV_BGR2GRAY);
    cv::equalizeHist(image,image);
    //获取到的人脸（可以是多个）
    vector<cv::Rect> faces;
    //开始识别
    classifier.detectMultiScale(image,faces,1.1,3,0,cv::Size(50,50));
    //有人脸图像
    if(faces.size())
    {
        //取第一个人脸图像给face引用
        face = faces[0];
        return 1;
    }else
    {
        return 0;

    }

}

int MyFace::recognize(cv::Mat face, Employee &e)
{
    //设置百度的连接信息
    aip::Face *client;
    //填写自己的key
    client = new aip::Face("","","");
    //设置缓冲区
    vector<unsigned char> buf;
    //将图像压缩为jpg格式到buf缓冲区中
    imencode(".jpg",face,buf);
    //压缩图像后转换成百度需要的base64编码
    string faceimg = aip::base64_encode((char*)buf.data(),buf.size());
    //开始在百度自定义人脸库中搜索，返回结果json
    Json::Value json = client->search(faceimg,"BASE64", "group1",aip::null);
    /*
    Json::StyledWriter writer;
    qDebug()<<"hello";
    string temp_str = writer.write(json);
    QString qstr = QString::fromStdString(temp_str);
    //qDebug()<<qstr;
    string temp_str2 = json.toStyledString();
    qstr = QString::fromStdString(temp_str2);
    cout<<temp_str2<<endl;
    */
    //获取识别结果
    string msg = json["error_msg"].asString();
    //如果识别成功就将信息添加到接收的结构体
    if(msg == "SUCCESS")
    {
        qDebug()<<"识别成功了";
        e.id = json["result"]["user_list"][0]["user_id"].asString();
        e.depart_id = json["result"]["user_list"][0]["group_id"].asString();
        e.info = json["result"]["user_list"][0]["user_info"].asString();
        e.score = json["result"]["user_list"][0]["score"].asDouble();
        return 1;
    }else
    {
        return 0;
    }
}
