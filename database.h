#ifndef DATABASS_H
#define DATABASS_H

#include <QDebug>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlQueryModel>

#include <QString>
#include <QDateTime>

#include <ctime>


class Databass
{
public:
    Databass();
    ~Databass();
    int punch(std::string id, time_t time);
    int getTableColoum();

    int insertEmployeeInfo(std::string id,std::string name);
    int getname(std::string id,std::string & name);

private:
    QSqlDatabase db;
    QString tableName;

    void createTable();
    void openDb();

    QString getTime();
};

#endif // DATABASS_H
