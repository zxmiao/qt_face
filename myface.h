#ifndef FACE_H
#define FACE_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>


using namespace cv;

struct Employee
{
    std::string depart_id; //<部门
    std::string id; //< 工号
    std::string info; //< 员工信息
    double score; //< 相似度得分
};

typedef int (*face_callback_t)(Employee& e, void* data);
class MyFace
{
public:
    MyFace(std::string model);

    /**
         * 检测图像中是否存在人脸
         * @param image 待检测图像
         * @param face 人脸的范围
         * @retval 1 找到人脸
         * @retval 0 没有找到人脸
         */
    int detect(cv::Mat image, cv::Rect& face);

    /**
         * 识别人脸
         * 创建线程，将人脸图像进行压缩和BASE64编码后发送给百度云
         * 解析百度云API返回的结果：
         * 1. 如果识别成功，调用回调函数，并将结果传给回调函数。
         * 2. 如果识别失败，不调用回调函数。
         * @param face 待识别的人脸图像
         * @param callback 回调函数
         * @param data 回调函数的第二个参数
         */
    int recognize(cv::Mat face, Employee& e);
private:
   cv::CascadeClassifier classifier;
   cv::Mat image;

};

#endif // FACE_H
